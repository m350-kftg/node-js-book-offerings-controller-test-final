import mongoose from 'mongoose';

const offeringSchema = new mongoose.Schema({
  vendor: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
});

const model = mongoose.model('offerings', offeringSchema);

export { model };
