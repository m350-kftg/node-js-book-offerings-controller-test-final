import {
  getOfferings,
  createOffering,
  updateOffering,
  removeOffering,
} from '../offering/offering.controller.js';
import { model as Offering } from '../offering/offering.model.js';

jest.mock('../offering/offering.model.js');

// Gemockte Response, welche der Controller übergeben wird
const response = {
  status: jest.fn((x) => x),
  json: jest.fn((x) => x),
};

describe('Getting offerings', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test('Loading offerings successfully', async () => {
    // given - Vorbereiten von erfolgreichem Laden von Angeboten
    //         Mocking von Model
    const storedOfferings = [
      { id: '1', vendor: 'orellfuessli.ch', price: 45 },
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    Offering.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockResolvedValue(storedOfferings),
      };
    });

    // when - Angebote laden
    await getOfferings(null, response);

    // then - überprüfen, ob korrekte Angebote
    // geladen wurden
    expect(response.json).toHaveBeenCalledWith(storedOfferings);
  });

  test('Loading offerings unsuccessfully', async () => {
    // given - Vorbereiten von fehlgeschlagenen Laden von Angeboten
    //         Mocking von Model
    Offering.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockRejectedValue({ message: 'DB error' }),
      };
    });

    // when - Angebote laden
    await getOfferings(null, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: 'DB error' });
  });
});

describe('Creating offerings', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Gemockter Request, welcher Controller übergeben wird
  const request = {
    body: {
      vendor: 'orellfuessli.ch',
      price: 45,
    },
  };

  test('Creating offering successfully', async () => {
    // given - Vorbereiten von erfolgreichem Speichern von Angeboten
    //         Mocking von Model
    const storedOffering = {
      id: '1',
      vendor: request.body.vendor,
      price: request.body.price,
    };
    Offering.create.mockResolvedValue(storedOffering);

    // when - Angebote laden
    await createOffering(request, response);

    // then - überprüfen, ob korrekte Angebote geladen wurden
    expect(response.json).toHaveBeenCalledWith(storedOffering);
  });

  test('Creating offerings unsuccessfully', async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Offering.create.mockRejectedValue({ message: 'DB error' });

    // when - Angebote laden
    await createOffering(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: 'DB error' });
  });
});

describe('Updating offerings', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Gemockter Request, welcher Controller übergeben wird
  const request = {
    params: {
      id: '1',
    },
    body: {
      vendor: 'orellfuessli.ch',
      price: 45,
    },
  };

  test('Updating offering successfully', async () => {
    // given - Vorbereiten von erfolgreichem Speichern von Angeboten
    //         Mocking von Model
    const offeringToUpdate = {
      id: '1',
      vendor: 'saxbooks.ch',
      price: 42,
      save: jest.fn().mockResolvedValue({
        id: '1',
        vendor: request.body.vendor,
        price: request.body.price,
      }),
    };

    Offering.findById.mockImplementation(() => {
      return {
        exec: jest.fn().mockResolvedValue(offeringToUpdate),
      };
    });

    // when - Angebote laden
    await updateOffering(request, response);

    // then
    // 1. überprüfen, ob geladenes Angebot korrekt angepasst wird
    expect(offeringToUpdate.vendor).toBe(request.body.vendor);
    expect(offeringToUpdate.price).toBe(request.body.price);
    // 2. überprüfen, ob angepasstes Angebot response hinzugefügt wird
    expect(response.json).toHaveBeenCalledWith({
      id: request.params.id,
      vendor: request.body.vendor,
      price: request.body.price,
    });
  });

  test('Updating offering fails because id is missing', async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Offering.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockResolvedValue(null),
      };
    });

    // when - Angebote laden
    await updateOffering(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.json).toHaveBeenCalledWith({
      message: 'Offering with id 1 not found.',
    });
  });

  test('Updating offering fails because of a db error', async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Offering.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockRejectedValue({ message: 'DB error' }),
      };
    });

    // when - Angebote laden
    await updateOffering(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: 'DB error' });
  });
});

describe('Deleting offerings', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Gemockter Request, welcher Controller übergeben wird
  const request = {
    params: {
      id: '1',
    },
    body: {
      vendor: 'orellfuessli.ch',
      price: 45,
    },
  };

  test('Deleting offering successfully', async () => {
    // given - Vorbereiten von erfolgreichem Speichern von Angeboten
    //         Mocking von Model
    const deletedOffering = {
      id: '1',
      vendor: request.body.vendor,
      price: request.body.price,
    };
    Offering.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockResolvedValue({
          deleteOne: jest.fn().mockResolvedValue({
            id: id,
            vendor: request.body.vendor,
            price: request.body.price,
          }),
        }),
      };
    });

    // when - Angebote laden
    await removeOffering(request, response);

    // then - überprüfen, ob korrekte Angebote geladen wurden
    expect(response.json).toHaveBeenCalledWith(deletedOffering);
  });

  test('Deleting offering fails because id is missing', async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Offering.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockResolvedValue(null),
      };
    });

    // when - Angebote laden
    await removeOffering(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.json).toHaveBeenCalledWith({
      message: 'Offering with id 1 not found.',
    });
  });

  test('Deleting offering fails because of a db error', async () => {
    // given - Vorbereiten von fehlgeschlagenen Speichern von Angeboten
    //         Mocking von Model
    Offering.findById.mockImplementation((id) => {
      return {
        exec: jest.fn().mockRejectedValue({ message: 'DB error' }),
      };
    });
    // when - Angebote laden
    await removeOffering(request, response);

    // then - überprüfen, ob richtiger Fehler aufgetreten ist
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: 'DB error' });
  });
});
